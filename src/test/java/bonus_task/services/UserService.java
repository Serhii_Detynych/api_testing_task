package bonus_task.services;

import bonus_task.models.User;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;

public class UserService {
    private final String endpoint = "http://localhost:8080/api/users";
    private final String singleUserEndpoint = endpoint + "/{id}";

    public List<User> getAllUsers() {
        Response response = given()
                .when()
                .get(endpoint);

        return response.jsonPath().getList("users", User.class);
    }

    public User createUser(String email, String firstName, String lastName) {
        User user = new User(email, firstName, lastName);

        Response response = given()
                .header("Content-type", "application/json")
                .body(user)
                .when()
                .post(endpoint);
        return response.as(User.class);
    }

    public User getUserById(int id) {
        Response response = given()
                .when()
                .get(singleUserEndpoint, id);
        return response.as(User.class);
    }

    public User updateUser(int id, String email, String firstName, String lastName) {
        User user = new User(email, firstName, lastName);
        Response response = given()
                .header("Content-type", "application/json")
                .body(user)
                .when()
                .put(singleUserEndpoint, id);
        return response.as(User.class);
    }

    public User deleteUser(int id) {
        Response response = given()
                .when()
                .delete(singleUserEndpoint, id);
        return response.as(User.class);
    }
}
