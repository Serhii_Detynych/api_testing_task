package bonus_task.services;

import bonus_task.models.Track;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;

public class TrackService {
    private final String endpoint = "http://localhost:8080/api/tracks";
    private final String singleTrackEndpoint = endpoint + "/{id}";

    public List<Track> getAllTracks() {
        Response response = given()
                .when()
                .get(endpoint);
        return response.jsonPath().getList("tracks", Track.class);
    }

    public Track createTrack(String album, String artist, int duration, String title, int year) {
        Track track = new Track(album,artist, duration,title,year);
        Response response = given()
                .header("Content-type", "application/json")
                .body(track)
                .when()
                .post(endpoint);
        return response.as(Track.class);
    }

    public Track getTrackById(int id) {
        Response response = given()
                .when()
                .get(singleTrackEndpoint, id);
        return response.as(Track.class);
    }

    public Track updateTrack(int id, String album, String artist, int duration, String title, int year) {
        Track track = new Track(album,artist, duration,title,year);
        Response response = given()
                .header("Content-type", "application/json")
                .body(track)
                .when()
                .put(singleTrackEndpoint, id);
        return response.as(Track.class);
    }

    public Track deleteTrack(int id) {
        Response response = given()
                .when()
                .delete(singleTrackEndpoint, id);
        return response.as(Track.class);
    }

}
