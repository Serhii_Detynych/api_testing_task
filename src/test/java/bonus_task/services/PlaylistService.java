package bonus_task.services;

import bonus_task.models.Playlist;
import bonus_task.models.Track;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;

public class PlaylistService {
    private final String endpoint = "http://localhost:8080/api/playlists";
    private final String singlePlaylistEndpoint = endpoint + "/{id}";

    public List<Playlist> getAllPlaylists() {
        Response response = given()
                .when()
                .get(endpoint);
        return response.jsonPath().getList("playlists", Playlist.class);
    }

    public Playlist createPlaylist(String description, boolean isPublic, String name, int userId) {
        Playlist playlistToCreate = new Playlist(description, isPublic, name, userId);
        Response response = given()
                .header("Content-type", "application/json")
                .body(playlistToCreate)
                .when()
                .post(endpoint);
        return response.as(Playlist.class);
    }

    public Playlist getPlaylistById(int id) {
        Response response = given()
                .when()
                .get(singlePlaylistEndpoint, id);
        return response.as(Playlist.class);
    }

    public Playlist updatePlaylist(int id, String description,boolean isPublic, String name) {
        Playlist playlistToUpdate = getPlaylistById(id);
        int userId = playlistToUpdate.getUserId();
        playlistToUpdate = new Playlist(description, isPublic, name, userId);
        Response response = given()
                .header("Content-type", "application/json")
                .body(playlistToUpdate)
                .when()
                .put(singlePlaylistEndpoint, id);
        return response.as(Playlist.class);
    }

    public Playlist deletePlaylist(int id) {
        Response response = given()
                .when()
                .delete(singlePlaylistEndpoint, id);
        return response.as(Playlist.class);
    }

    public List<Track> getTracksOfPlaylist(int id) {
        String tracksEndpoint = singlePlaylistEndpoint + "/tracks";
        Response response = given()
                .when()
                .get(tracksEndpoint, id);
        return response.getBody().as(new TypeRef<>() {});
    }

    public Playlist addTrackToPlaylist(int playListId, int trackId) {
        String addTrackEndpoint = singlePlaylistEndpoint + "/tracks/add";
        String body = "{" +
                "\"trackId\": " + trackId +
                "}";
        Response response = given()
                .header("Content-Type", "application/json")
                .body(body)
                .when()
                .post(addTrackEndpoint, playListId);

        return response.as(Playlist.class);
    }
    public Playlist deleteTrackFromPlaylist(int playListId, int trackId) {
        String removeTrackEndpoint = singlePlaylistEndpoint + "/tracks/remove";
        String body = "{" +
                "\"trackId\": " + trackId +
                "}";
        Response response = given()
                .header("Content-Type", "application/json")
                .body(body)
                .when()
                .delete(removeTrackEndpoint, playListId);

        return response.as(Playlist.class);
    }
}
