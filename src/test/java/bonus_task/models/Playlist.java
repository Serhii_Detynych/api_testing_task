package bonus_task.models;

import java.util.List;
import java.util.Objects;

public class Playlist {
    private String description;
    private int id;
    private boolean isPublic;
    private String name;
    private List<Track> tracks;
    private int userId;

    public Playlist(String description, boolean isPublic, String name, int userId) {
        this.description = description;
        this.isPublic = isPublic;
        this.name = name;
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return "Playlist{" +
                "description='" + description + '\'' +
                ", id=" + id +
                ", isPublic=" + isPublic +
                ", name='" + name + '\'' +
                ", tracks=" + tracks +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Playlist playlist = (Playlist) object;
        return id == playlist.id && isPublic == playlist.isPublic && userId == playlist.userId && Objects.equals(description, playlist.description) && Objects.equals(name, playlist.name) && Objects.equals(tracks, playlist.tracks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, id, isPublic, name, tracks, userId);
    }
}
