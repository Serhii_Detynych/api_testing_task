package bonus_task.models;

public class Track {
    private int id;
    private String title;
    private String artist;
    private int duration;
    private int year;
    private String album;

    public Track() {
    }

    public Track(String title, String artist, int duration, int year) {
        this.title = title;
        this.artist = artist;
        this.duration = duration;
        this.year = year;
    }

    public Track(String album, String artist, int duration, String title, int year) {
        this.album = album;
        this.artist = artist;
        this.duration = duration;
        this.title = title;
        this.year = year;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Track{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", duration=" + duration +
                ", year=" + year +
                ", album='" + album + '\'' +
                '}';
    }
}
