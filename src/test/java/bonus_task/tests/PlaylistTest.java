package bonus_task.tests;

import bonus_task.models.Playlist;
import bonus_task.services.PlaylistService;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PlaylistTest {
    private PlaylistService playlistService;

    @BeforeEach
    public void setUp() {
        playlistService = new PlaylistService();
    }

    @Test
    @Order(1)
    public void createPlaylistTest() {
        Playlist createdPlaylist = playlistService.createPlaylist("NewPlaylist", true, "Playlist", 3);
        assertEquals("NewPlaylist", createdPlaylist.getDescription());
        assertTrue(createdPlaylist.isPublic());
        assertEquals("Playlist", createdPlaylist.getName());
        assertEquals(3, createdPlaylist.getUserId());
    }

    @Test
    @Order(2)
    public void updatePlaylistTest() {
        Playlist updatedPlaylist = playlistService.updatePlaylist(2,"Updated description", false, "Updated name");
        assertEquals("Updated description", updatedPlaylist.getDescription());
        assertEquals("Updated name", updatedPlaylist.getName());
        assertFalse(updatedPlaylist.isPublic());
    }

    @Test
    @Order(3)
    public void getPlaylistByIdTest() {
        Playlist playlist = playlistService.getPlaylistById(1);

        assertEquals(1, playlist.getId());
        assertEquals("Favourite", playlist.getName());
        assertEquals("All track that I like", playlist.getDescription());
        assertFalse(playlist.isPublic());
        assertEquals(1, playlist.getUserId());
    }


    @Test
    @Order(4)
    public void deletePlaylistTest(){
        Playlist deletedPlaylist = playlistService.deletePlaylist(2);
        List<Playlist> playlists = playlistService.getAllPlaylists();
        assertFalse(playlists.contains(deletedPlaylist));
    }
}
