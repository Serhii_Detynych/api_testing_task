package main_task.tests;

import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JSonPlaceholderTest {

    private static final String BASE_URL = " https://jsonplaceholder.typicode.com/";
    private static final String USERS_ENDPOINT = "users";

    @Test
    public void testVerifyStatusCode() {
        String usersEndpoint = BASE_URL + USERS_ENDPOINT;

        given()
                .when()
                .get(usersEndpoint)
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testVerifyResponseHeader() {
        String usersEndpoint = BASE_URL + USERS_ENDPOINT;
        Response response =  given()
                .when()
                .get(usersEndpoint);

        String rpContentTypeHeader = response.getHeader("Content-Type");
        assertEquals(rpContentTypeHeader, "application/json; charset=utf-8");
    }


    @Test
    public void testVerifyResponseBody() {
        String usersEndpoint = BASE_URL + USERS_ENDPOINT;
        Response response =  given()
                .when()
                .get(usersEndpoint);

        given()
                .when()
                .get(usersEndpoint)
                .then().log().body();

        int expectedUserCount = 10;
        int actualUserCount = response.jsonPath().getList("$").size();
        assertEquals(actualUserCount, expectedUserCount);
    }

}
